+++
author = "Tobias Berg"
title = "I Created a Small Toolbox for Converting Data"
description = "Just a tiny collection of conversion tools"
date = "2023-02-21"
+++

After having used various random online tools for converting data from one format to another, I finally build my own.<!--more-->

I often find myself converting a JSON structure to Go or Typescript, but it always made me feel a little uncomfortable.
Sending my random data to a website where I don't know the owner, and I don't know if it could be stored somewhere, wasn't the best idea.

I've built a small site with these same, so I could run it myself on my small server in my appartment.

This was done over a few days, and I will probably expand it with more random tools when I need them.
It's open source, and you can host it yourself if you want, then you're in complete control.

It currently has the following tools:

- JSON to Go
- JSON Validate
- JSON to TypeScript

These are planned:

- Go to TypeScript
- To and from Base64

You can find the code here: https://gitlab.com/TobiasBerg/dev-toolbox
