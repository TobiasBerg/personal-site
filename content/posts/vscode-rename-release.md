+++
author = "Tobias Berg"
title = "Touchbar Rename Extension for vscode"
description = "Visual Studio Code recently released support for the touchBar on macOS."
date = "2017-10-10"
+++

Visual Studio Code recently released support for the touchBar on macOS.<!--more-->

Ever since getting the new Macbook Pro with touchBar, doing things like refactor rename in webStorm has been a pain, so when I saw this news I jumped on vscode right away to test it out and see if it is finally better than webStorm.

To make it easier to rename things in vscode I have created a tiny extension which adds a simple button on the touchBar which lets me access the rename function faster.

Check out the code here: 
[vscode-touchbar-rename](https://github.com/TobiasBerg/vscode-touchbar-rename)
