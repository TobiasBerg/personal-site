+++
author = "Tobias Berg"
title = "The blog is online again"
description = "The blog is finally up again after being down for quite a while."
date = "2014-01-04"
+++

The blog is finally up again after being down for quite a while.<!--more-->
After thinking about writing a new blog from scratch I decided to try out Jekyll hosted on Github pages instead.
I’m working on some essays but I’m not sure when they’ll be ready.

Until then stay tuned!