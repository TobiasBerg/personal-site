+++
author = "Tobias Berg"
title = "Hosting Private Sites over HTTPS Using Caddy and Cloudflare"
description = "Small guide on how to host non-public sites over HTTPS using Caddy and Cloudflare"
date = "2023-02-25"
+++

I've been running a small server in my appartment for a while. One of the things I've been using it for is as a remote development host, 
meaning all my personal project are stored and hosted on it. I also host a few apps like Home assistant and Grafana.<!--more-->

## How to

When starting to host sites on my devbox, an issue I ran into was not being able to serve projects over HTTPS.

Since the server can't be reached over HTTPS publicly, Caddy doesn't have a way to generate certificates and get them verified.

To overcome this we can use the Cloudflare API as a sort of intermediary, so we can reply to challenges, when caddy wants to issue a certificate.
Luckily Caddy has modules to make this setup extremely easy.

1. [Download the Caddy binary](https://caddyserver.com/download?package=github.com%2Fcaddy-dns%2Fcloudflare) with Cloudflare installed
2. Overwrite Caddy in /usr/bin
    - Remember to chown as root and chmod +x
3. Create a [Cloudflare API token](https://developers.cloudflare.com/fundamentals/api/get-started/create-token/)
    - It only needs permission to edit DNS
4. Update your Caddyfile with cloudflare config
    ```Caddyfile
      domain.name {
        tls {
          dns cloudflare <API_TOKEN>
        }
        ...
      }
      ```
5. Restart Caddy

Caddy can now generate valid certificates for your non-public sites, enabling you to serve them over HTTPS.

### Resources: 

- [This wiki page](https://caddy.community/t/how-to-use-dns-provider-modules-in-caddy-2/8148) describes the Caddy + Cloudflare integration well.
