Full stack engineer building cool things with friends.

Currently working at [LootLocker](https://www.lootlocker.io/)

To learn more about me you can check out my [Last.fm](https://www.last.fm/user/TobiasBerg) to see what I'm listening to.

If you want to get in touch you can reach me on twitter [@TobiasBerg](https://twitter.com/TobiasBerg)
